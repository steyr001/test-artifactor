# Artifactory Docker Compose
## The services version are:
- Postgresql 9.5.2
- Artifactory OSS 5.4.5
- Nginx latest

## Docker-Compose Usage
To run of the compose, you should execute:  
```bash
$ docker-compose up -d
```
