# Setup Artifactory in kubernetes

### Requirements
- [Jenkins](https://jenkins.io/)
- [Maven Integration plugin](https://wiki.jenkins.io/display/JENKINS/Maven+Project+Plugin)
- [Kubernetes](https://kubernetes.io/)
- [Docker](https://www.docker.com/)

### Install jenkins job
1. Install Maven Integration plugin in Jenkins
2. Add "Maven" 
 - Go to Manage Jenkins/Global Tool Configuration
 - Press button "Add Maven"
 - Enter the name MAVEN
 - Press Save
2. Create Pipeline item in jenkins
3. Add following parameters:
  - IP_CLUSTER
  - LOGIN
  - PASSWORD
4. Add pipeline script
5. Press Save
6. Press Build with Parameters
7. Enter the Ip,login and password from your cluster  
